package com.cellulant.requestrouter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RequestRouterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RequestRouterApplication.class, args);
    }

}
