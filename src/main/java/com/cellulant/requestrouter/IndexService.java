package com.cellulant.requestrouter;

import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by kamauwamatu
 * Project request-router
 * User: kamauwamatu
 * Date: 2019-08-17
 * Time: 15:17
 */
@Service
public class IndexService {

    private RestTemplate restTemplate;
    private Logger logger;
    private ObjectMapperConfiguration objectMapper;

    /**
     * Instantiates a new Index service.
     *
     * @param restTemplate the rest template
     * @param logger       the logger
     * @param objectMapper
     */
    public IndexService(RestTemplate restTemplate, Logger logger, ObjectMapperConfiguration objectMapper) {
        this.restTemplate = restTemplate;
        this.logger = logger;
        this.objectMapper = objectMapper;
    }

    /**
     * Process request object.
     *
     * @param protocol     the protocol
     * @param url          the url
     * @param request      the request
     * @param headerString the header string
     * @param jwt          the jwt
     * @return the object
     */
    public Object processRequest(String protocol, String url, Object request, String headerString, String jwt, String encryptionType) throws Exception {

        HttpHeaders headers = new HttpHeaders();

        encryptionType = (null != encryptionType) ? encryptionType : "0";
        headers.add("x-headers", headerString);
        headers.add("Authorization", jwt);
        headers.add("encryption-type", encryptionType);
        headers.add("Content-Type", "application/json");


        Object obj;
        if (protocol.equalsIgnoreCase("post")) {
            HttpEntity<Object> payload = new HttpEntity<>(request, headers);
            if (encryptionType.trim().equals("2") || encryptionType.trim().equals("1")) {
                obj = restTemplate.exchange(url, HttpMethod.valueOf(protocol.toUpperCase()), payload, String.class).getBody();
            } else {
                obj = restTemplate.exchange(url, HttpMethod.valueOf(protocol.toUpperCase()), payload, Object.class).getBody();
            }
            logger.info("RESPONSE :: {} ", obj);
        } else {
            HttpEntity<Object> payload = new HttpEntity<>(null, headers);
            obj = restTemplate.exchange(url, HttpMethod.valueOf(protocol.toUpperCase()), payload, String.class).getBody();
            logger.info("RESPONSE :: {} ", obj);
        }

        return obj;
    }

    /**
     * Process request object.
     *
     * @param protocol the protocol
     * @param url      the url
     * @param request  the request
     * @return the object
     */
    public Object processRequest(String protocol, String url, Object request) throws Exception {
        Object obj;

        if (protocol.equalsIgnoreCase("post")) {
            obj = restTemplate.exchange(url, HttpMethod.valueOf(protocol.toUpperCase()), new HttpEntity<>(request), Object.class).getBody();
            logger.info("RESPONSE :: {} ", objectMapper.objectMapper().writeValueAsString(obj));
        } else {
            obj = restTemplate.exchange(url, HttpMethod.valueOf(protocol.toUpperCase()), new HttpEntity(null), Object.class).getBody();
            logger.info("RESPONSE :: {} ", objectMapper.objectMapper().writeValueAsString(obj));
        }


        return obj;
    }
}
