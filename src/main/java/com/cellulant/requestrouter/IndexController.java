package com.cellulant.requestrouter;

import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by kamauwamatu
 * Project request-router
 * User: kamauwamatu
 * Date: 2019-08-17
 * Time: 14:00
 */
@RestController
public class IndexController {

    private Logger logger;
    private Configs configs;
    private IndexService indexService;

    /**
     * Instantiates a new Index controller.
     *
     * @param logger       the logger
     * @param configs      the configs
     * @param indexService
     */
    public IndexController(Logger logger, Configs configs, IndexService indexService) {
        this.logger = logger;
        this.configs = configs;
        this.indexService = indexService;
    }


    /**
     * Route request response entity.
     *
     * @param object  the object
     * @param headers the headers
     * @param request the request
     * @return the response entity
     */
    @RequestMapping("request-router/**")
    public ResponseEntity<?> routeRequest(@RequestBody(required = false) String object, @RequestHeader(value = "Authorization", required = false) String jwt, @RequestHeader(value = "x-headers", required = false) String headers, @RequestHeader(value = "encryption-type", required = false) String encryptionType, HttpServletRequest request) {

        String absolutePath = request.getRequestURI();
        String protocol = request.getMethod();
        String apiValue = null;
        String restOfValue = null;
        String newPath = null;
        Object returnValue = null;


        try {

            logger.info("ABSOLUTE PATH VALUE :: {} ", absolutePath);

            apiValue = absolutePath.split("/")[2].toLowerCase();

            logger.info("API VALUE :: {} ", apiValue);
            restOfValue = absolutePath.replace("/request-router/", "");

            logger.info("REST OF VALUE :: {} ", restOfValue);

            switch (apiValue) {


                case "authentication-service":

                    logger.info("ENTER AUTHENTICATION SERVICE ");
                    // Fetch the URL

                    returnValue = indexService.processRequest(protocol, configs.getAuthenticationServiceUrl(), object);
                    // Call the coresponding API.
                    break;
                case "encryption-handler":
                    restOfValue = restOfValue.replace("/encryption-handler/", "");
                    restOfValue = restOfValue.replace("//", "/");

                    logger.info("ENTER ENCRYPTION SERVICE ");
                    newPath = configs.getEncryptionHandlerUrl() + restOfValue;
                    logger.info("FINAL PATH  {} ", newPath);
                    returnValue = indexService.processRequest(protocol, newPath, object);
                    break;
                case "api-gateway":

                    logger.info("ENTER GATEWAY SERVICE URL :: {} ", restOfValue);
                    restOfValue = restOfValue.replace("api-gateway/", "");
                    logger.info("UPDATED GATEWAY SERVICE URL :: {} ", restOfValue);
                    newPath = configs.getApiGatewayUrl() + restOfValue;

                    logger.info("FINAL PATH  {} ENCRYPTION VALUE :: {} HEADERS :: {} ", newPath, encryptionType, headers);
                    returnValue = indexService.processRequest(protocol, newPath, object, headers, jwt, encryptionType);
                    break;
                default:

                    break;
            }

        } catch (Exception ec) {

            logger.info("Path is not configured");

        }


        return new ResponseEntity<>(returnValue, HttpStatus.OK);
    }


}
