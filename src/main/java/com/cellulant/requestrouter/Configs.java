package com.cellulant.requestrouter;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by kamauwamatu
 * Project request-router
 * User: kamauwamatu
 * Date: 2019-08-17
 * Time: 14:25
 */
@Configuration
@Data
@ConfigurationProperties("request.router")
public class Configs {

    private String authenticationServiceUrl;
    private String encryptionHandlerUrl;
    private String apiGatewayUrl;
    private int globalTimeout;

}
